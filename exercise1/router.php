<?php

	/**
	 * Router Class
	 *
	 * Parses URIs and determines routing. This is an adaptation of routing
	 * that I currently use in my everyday projects.
	 */
	class Router {

		var $uri;
		var $segments;
		var $directory;
		var $class			= '';
		var $method			= '';
		var $defaultMethod	= 'index';

		/**
		 * Constructor
		 *
		 * Runs the route mapping function.
		 */
		function __construct(
			$directory = null
			)
		{
			$this->set_directory($directory);

			$this->uri = $this->_detect_uri();

			$this->_set_controller();
		}

		private function _set_controller()
		{
			if (strpos($this->uri, '/') !== FALSE)
			{
				$uri = explode('/', $this->uri);
				$this->_set_request($uri);
			}
			else
			{
				$this->_set_request(array($this->uri));
			}
		}

		private function _detect_uri()
		{
			if ( ! isset($_SERVER['REQUEST_URI']))
			{
				return '';
			}

			$uri = $_SERVER['REQUEST_URI'];
			if (strpos($uri, $_SERVER['SCRIPT_NAME']) === 0)
			{
				$uri = substr($uri, strlen($_SERVER['SCRIPT_NAME']));
			}
			elseif (strpos($uri, dirname($_SERVER['SCRIPT_NAME'])) === 0)
			{
				$uri = substr($uri, strlen(dirname($_SERVER['SCRIPT_NAME'])));
			}

			if (strncmp($uri, '?/', 2) === 0)
			{
				$uri = substr($uri, 2);
			}
			$parts = preg_split('#\?#i', $uri, 2);
			$uri = $parts[0];
			if (isset($parts[1]))
			{
				$_SERVER['QUERY_STRING'] = $parts[1];
				parse_str($_SERVER['QUERY_STRING'], $_GET);
			}
			else
			{
				$_SERVER['QUERY_STRING'] = '';
				$_GET = array();
			}
			$uri = parse_url($uri, PHP_URL_PATH);

			// Do some final cleaning of the URI and return it
			return str_replace(array('//', '../'), '/', trim($uri, '/'));
		}

		function _set_request($segments = array())
		{
			$this->segments = $segments;
			$this->set_class($this->segments[0]);

			if (isset($this->segments[1]))
			{
				// Set the action method
				$this->set_method($this->segments[1]);
			}
			else
			{
				// Use the default method if none is provided
				$this->set_method($this->defaultMethod);
			}
		}

		function set_class($class)
		{
			$this->class = str_replace(array('/', '.'), '', $class);
		}

		function get_class()
		{
			return $this->class;
		}

		function set_method($method)
		{
			$this->method = $method;
		}

		function get_method()
		{
			return $this->method;
		}

		function set_directory($directory)
		{
			$this->directory = $directory;
		}

		function get_directory()
		{
			return $this->directory;
		}

	}

	// router.php?/foo/bar
	$router = new Router();

	// Load the local application controller
	if ( ! file_exists($router->get_directory() . $router->get_class().'.php')) {
		die('Unable to load controller. Please make sure the class and action methods exist.');
	}

	include($router->get_directory() . $router->get_class().'.php');

	$class  = ucwords($router->get_class() . 'Controller');
	$method = $router->get_method() . 'Action';

	if ( ! class_exists($class)
		OR strncmp($method, '_', 1) == 0
		)
	{
		die("Unable to find controller/method: {$class}/{$method}");
	}

	// Load class
	$class = new $class();

	// Validate the action method exists
	if ( ! in_array(strtolower($method), array_map('strtolower', get_class_methods($class)))) {
		die("Unable to find method in '{$class}': {$method}");
	}

	// Call the requested method.
	call_user_func_array(array(&$class, $method), array_slice($router->segments, 2));

?>