<?php

function detect_uri()
{
	if ( ! isset($_SERVER['REQUEST_URI']))
	{
		return '';
	}

	$uri = $_SERVER['REQUEST_URI'];
	if (strpos($uri, $_SERVER['SCRIPT_NAME']) === 0)
	{
		$uri = substr($uri, strlen($_SERVER['SCRIPT_NAME']));
	}
	elseif (strpos($uri, dirname($_SERVER['SCRIPT_NAME'])) === 0)
	{
		$uri = substr($uri, strlen(dirname($_SERVER['SCRIPT_NAME'])));
	}

	if (strncmp($uri, '?/', 2) === 0)
	{
		$uri = substr($uri, 2);
	}
	$parts = preg_split('#\?#i', $uri, 2);
	$uri = $parts[0];
	if (isset($parts[1]))
	{
		$_SERVER['QUERY_STRING'] = $parts[1];
		parse_str($_SERVER['QUERY_STRING'], $_GET);
	}
	else
	{
		$_SERVER['QUERY_STRING'] = '';
		$_GET = array();
	}
	$uri = parse_url($uri, PHP_URL_PATH);

	// Do some final cleaning of the URI and return it
	return str_replace(array('//', '../'), '/', trim($uri, '/'));
}

class FooController {
    function barAction() {
        print "hello";
    }
}

// router-simplified.php?/foo/bar
$url = detect_uri();

preg_match_all('#([^/]+)#',$url,$m);

$a = ucfirst((string)$m[1][0]) . 'Controller';

$c = new $a();

$c->{$m[1][1] . 'Action'}();

?>