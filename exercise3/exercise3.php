<?php

// Declare the driver interface
interface DataSource
{
    public function createData($data);
    public function fetchDataById($id);
    public function searchData($criteria);
}

class MysqlDataSource implements DataSource
{
    // ...
}


class PdoDataSource implements DataSource
{
    // ...
}