<?php
	function getCommonValues($array1, $array2) {
		// Flip array which will help 
	    $array1 = array_flip($array1);
	    $array2 = array_flip($array2);

	    $commonValues = array_diff_key($array1, $array2);

	    return $commonValues;
	}
	
	$diff = getCommonValues(
		array(1, 2, 4, 5, 6, 7, 8),
		array(1, 4, 6, 8, 0, 34)
	);

	var_dump($diff);